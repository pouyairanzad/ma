package com.sematec.googlemap.GoogleMap;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.sematec.googlemap.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnCameraIdleListener {

    private GoogleMap mMap;
    private static final int  Location_request=500;
    ArrayList<LatLng> listponits;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_maps );
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById( R.id.map );
        mapFragment.getMapAsync( this );
        listponits =new ArrayList<>(  );
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        try {
            boolean sucess = mMap.setMapStyle( MapStyleOptions.loadRawResourceStyle( this, R.raw.new_style ) );
        } catch (Exception ex) {
        }

        // Add a marker in Sydney and move the camera
        // LatLng sydney = new LatLng( -34, 151 );
        // mMap.addMarker( new MarkerOptions().position( sydney ).title( "Marker in Sydney" ) );
        // mMap.moveCamera( CameraUpdateFactory.newLatLng( sydney ) );
        LatLng coordinate = new LatLng( 35.864066, 50.977376

        );
        mMap.setOnCameraIdleListener( this );
        CameraUpdate location = CameraUpdateFactory.newLatLngZoom( coordinate, 15 );
        mMap.animateCamera( location );
        mMap.getUiSettings().setZoomControlsEnabled( true );
        mMap.getUiSettings().setMyLocationButtonEnabled( true );
        if (ActivityCompat.checkSelfPermission( this, Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission( this, Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions( this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},Location_request );
            return;
        }
        mMap.setMyLocationEnabled( true );
        mMap.setOnMapLongClickListener( new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                 if(listponits.size()==0){
                     listponits.clear();
                     mMap.clear();
                 }
                 listponits.add( latLng );
                 MarkerOptions markerOptions =new MarkerOptions();
                 markerOptions.position( latLng );
                 if (listponits.size()==1){
                     markerOptions.icon( BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN) );
                 }
                 else {
                     markerOptions.icon( BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED) );

                 }
                 mMap.addMarker( markerOptions );
                 if(listponits.size()==2){
                     String url = getRequesturl(listponits.get( 0 ),listponits.get( 1 ));
                     trackrequestdrictions trackrequestdrictions =new trackrequestdrictions();
                     trackrequestdrictions.execute( url );
                 }
            }
        } );
    }

    private String getRequesturl(LatLng orgin, LatLng dest) {
        String str_org="orgin"+orgin.latitude+","+orgin.longitude;
        String str_dest="dest"+dest.latitude+","+dest.longitude;
        String sensor="sensor=false";
        String mode="mode=driving ";
        String parm = orgin +"&"+dest+"&"+sensor+"&"+mode;
        String output="json";
        String url="https://maps.googleapis.com/maps/api/directions/"+output+"?"+parm;
        return  url;


    }
    private String requestdricetion(String requrl) {
         String responsestring="";
        InputStream inputStream =null;
        HttpURLConnection httpURLConnection=null;
        try{
            URL url =new URL( requrl );
            httpURLConnection =(HttpURLConnection) url.openConnection();
            httpURLConnection.connect();
            inputStream=httpURLConnection.getInputStream();
            InputStreamReader inputStreamReader=new InputStreamReader( inputStream );
            BufferedReader bufferedReader=new BufferedReader( inputStreamReader );

            StringBuffer stringBuffer =new StringBuffer( );
            String line ="";
            while ((line =bufferedReader.readLine())!=null){
                stringBuffer.append( line );

            }
            responsestring=stringBuffer.toString();
            bufferedReader.close();
            inputStreamReader.close();
        }
        catch (IOException ec){
            ec.printStackTrace();}
            finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            httpURLConnection.disconnect();
        }
        return  responsestring;

        }


    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode){
            case Location_request :
                if (grantResults.length > 0 && grantResults[0]==PackageManager.PERMISSION_GRANTED) {
                    mMap.setMyLocationEnabled(true );

                }
                break;
        }
    }
    public  class  trackrequestdrictions extends AsyncTask<String ,Void,String >{
        @Override
        protected String doInBackground(String... strings) {
            String responseString ="";
            responseString=requestdricetion( strings[0] );
            return responseString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute( s );
            Trackparser trackparser=new Trackparser();
            trackparser.execute( s );
        }
    }
    public class Trackparser extends  AsyncTask<String,Void,List<List<HashMap<String,String>>>>{

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... strings) {
            JSONObject jsonObject=null;
            List<List<HashMap<String,String>>> routs = null;
            try{
                jsonObject =new JSONObject( strings[0] );
                DirectionParser directionsJSONParser = new DirectionParser();
                routs =directionsJSONParser.parse( jsonObject);


            }
            catch (JSONException e){
                e.printStackTrace();
            }
            return routs;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> lists) {
            ArrayList ponits = null;
            PolylineOptions polylineOptions = null;
            for(List<HashMap<String,String>> path :lists){
                ponits=new ArrayList(  );
                polylineOptions=new PolylineOptions();
                for (HashMap<String,String> point: path){
                    double lat =Double.parseDouble( point.get( "lat" ) );
                    double lng =Double.parseDouble( point.get( "lon" ) );
                    ponits.add( new LatLng( lat,lng ) );

                }
                polylineOptions.addAll( ponits );
                polylineOptions.width( 15 );
                polylineOptions.color( Color.GREEN);
                polylineOptions.geodesic( true );
            }
            if(polylineOptions!=null){
                mMap.addPolyline( polylineOptions );

            }
            else {
                Toast.makeText( getApplicationContext(), "dricetion not found ", Toast.LENGTH_SHORT ).show();
            }

        }
    }

    @Override
    public void onCameraIdle() {
        double lat = mMap.getCameraPosition().target.latitude;
        double longi =mMap.getCameraPosition().target.longitude;
        Toast.makeText( this, "lat" + lat + "longi" + longi, Toast.LENGTH_SHORT ).show();

    }

}
