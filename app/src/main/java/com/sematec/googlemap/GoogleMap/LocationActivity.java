package com.sematec.googlemap.GoogleMap;

import android.Manifest;
import android.content.Context;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.location.DetectedActivity;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.sematec.googlemap.R;

import java.util.List;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.config.LocationParams;
import io.nlopez.smartlocation.location.providers.LocationBasedOnActivityProvider;

public class LocationActivity extends AppCompatActivity {
    Context mCotext=this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_location );
        cheackpermission();
        SmartLocation.with( this ).location().start( new OnLocationUpdatedListener() {
            @Override
            public void onLocationUpdated(Location location) {
                double lat =location.getLatitude();
                double langi=location.getLongitude();
                long cuurenttime=System.currentTimeMillis();
                Toast.makeText( mCotext, "lat"+lat+"long"+langi, Toast.LENGTH_SHORT ).show();


            }
        } );
    }
    void cheackpermission(){
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {/* ... */}

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {/* ... */}
        }).check();
    }
    void getActivity(){
        SmartLocation.with(this).location(new LocationBasedOnActivityProvider( new LocationBasedOnActivityProvider.LocationBasedOnActivityListener() {
            @Override
            public LocationParams locationParamsForActivity(DetectedActivity detectedActivity) {
                if (detectedActivity.getConfidence() >= 75) {
                    LocationParams.Builder builder = new LocationParams.Builder();
                    switch (detectedActivity.getType()) {
                        case DetectedActivity.IN_VEHICLE:

                            break;

                        case DetectedActivity.ON_BICYCLE:

                            break;
                        case DetectedActivity.WALKING:

                            break;
                    }
                    return builder.build();
                }
                return null;
            }
        })).start(new OnLocationUpdatedListener() {
            @Override
            public void onLocationUpdated(Location location) {
                //Do what you need here.
            }
        });
    }
}
