package com.sematec.googlemap;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.widget.Toast;

import com.google.android.gms.location.DetectedActivity;
import com.sematec.googlemap.MainActivity;
import com.sematec.googlemap.R;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.config.LocationParams;
import io.nlopez.smartlocation.location.providers.LocationBasedOnActivityProvider;

public class MyService extends Service {

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub


        SmartLocation.with( this ).location( new LocationBasedOnActivityProvider(
                new LocationBasedOnActivityProvider.LocationBasedOnActivityListener() {
                    @Override
                    public LocationParams locationParamsForActivity(DetectedActivity detectedActivity) {
                        if (detectedActivity.getConfidence() >= 75) {
                            LocationParams.Builder builder = new LocationParams.Builder();
                            switch (detectedActivity.getType()) {
                                case DetectedActivity.IN_VEHICLE:
                                    //builder.setInterval(/*Interval*/ )
                                          //  .setAccuracy(/*Locaiton Accuracy*/ );
                                    break;

                                case DetectedActivity.ON_BICYCLE:
                                    /* So on and so forth.... */

                                    break;
                            }
                            return builder.build();
                        }
                        return null;
                    }
                } ) ).start( new OnLocationUpdatedListener() {
            @Override
            public void onLocationUpdated(Location location) {
                //Do what you need here.
            }
        } );
        return startId;
    }

    }